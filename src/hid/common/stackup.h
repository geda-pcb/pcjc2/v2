#ifndef INCLUDED_STACKUP_H
#define INCLUDED_STACKUP_H

typedef struct {
  char *name;
  double z_top;
  double z_bottom;
  double thickness;
  char *color;
  bool solid; // True if (not sure what any more!)
  bool has_layer_group; // True if there is a layer_group defined
  Cardinal layer_group; // NB: This is a layer _group_ (not _layer_)
  bool has_outline_layer; // True if there is an outline_layer defined
  Cardinal outline_layer; // NB: This is a _layer_ not a layer group.

} stack_item_t;

typedef struct {
  stack_item_t *items;
  int item_count;
} stackup_t;


stackup_t *calculate_stackup (void);
void free_stackup (stackup_t *stackup);

#endif
