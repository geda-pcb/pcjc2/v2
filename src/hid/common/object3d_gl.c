/* TODO ITEMS:
 *
 * Add PLINE simplification operation to consolidate co-circular segments for reduced geometry output.
 * Look at whether arc-* intersections can be re-constructed back to original geometry, not fall back to line-line.
 * Work on snap-rounding any edge which passes through the pixel square containing an any vertex (or intersection).
 * Avoid self-touching output in contours, where that self-touching instance creates two otherwise distinct contours or holes.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <glib.h>

#include "data.h"
#include "step_id.h"
#include "quad.h"
#include "vertex3d.h"
#include "contour3d.h"
#include "appearance.h"
#include "face3d.h"
#include "edge3d.h"
#include "object3d.h"
#include "polygon.h"
#include "rats.h"

#include "rtree.h"
#include "rotate.h"

#include "pcb-printf.h"
#include "misc.h"
#include "hid/hidint.h"

#define PERFECT_ROUND_CONTOURS
#define SUM_PINS_VIAS_ONCE
#define HASH_OBJECTS

#define DEBUG_EDGE_HIGHLIGHT
#undef DEBUG_EDGE_HIGHLIGHT


/* The Linux OpenGL ABI 1.0 spec requires that we define
 * GL_GLEXT_PROTOTYPES before including gl.h or glx.h for extensions
 * in order to get prototypes:
 *   http://www.opengl.org/registry/ABI/
 */
#ifndef WIN32
#  define GL_GLEXT_PROTOTYPES 1
#endif

/* This follows autoconf's recommendation for the AX_CHECK_GL macro
   https://www.gnu.org/software/autoconf-archive/ax_check_gl.html */
#if defined HAVE_WINDOWS_H && defined _WIN32
#  define WIN32_LEAN_AND_MEAN
#  include <windows.h>
#endif
#if defined HAVE_GL_GL_H
#  include <GL/gl.h>
#elif defined HAVE_OPENGL_GL_H
#  include <OpenGL/gl.h>
#else
#  error autoconf couldnt find gl.h
#endif

#ifdef WIN32
#  include "glext.h"

extern PFNGLUSEPROGRAMPROC         glUseProgram;
#endif


#include "hid_draw.h"
#include "hidgl.h"
#include "face3d_gl.h"
#include "object3d_gl.h"

//static Coord board_thickness;
#define HACK_BOARD_THICKNESS board_thickness
//#define HACK_BOARD_THICKNESS MM_TO_COORD(1.6)
#define HACK_COPPER_THICKNESS MM_TO_COORD(0.035)
#define HACK_PLATED_BARREL_THICKNESS MM_TO_COORD(0.08)
#define HACK_MASK_THICKNESS MM_TO_COORD(0.01)
#define HACK_SILK_THICKNESS MM_TO_COORD(0.01)

static GList *object3d_test_objects = NULL;

static appearance *object_default_face_appearance;
static appearance *object_debug_face_appearance;
static appearance *object_selected_face_appearance;
static appearance *object_default_edge_appearance;
static appearance *object_debug_edge_appearance;
static appearance *object_selected_edge_appearance;

void
object3d_test_init (void)
{
  object3d_test_objects = object3d_from_board_outline ();

  object_default_face_appearance = make_appearance();
  appearance_set_color (object_default_face_appearance, 0.8f, 0.8f, 0.8f); /* 1.0f */
  appearance_set_alpha (object_default_face_appearance, 1.0f);

  object_debug_face_appearance = make_appearance();
  appearance_set_color (object_debug_face_appearance, 1.0f, 0.0f, 0.0f); /* 0.5f */
  appearance_set_alpha (object_debug_face_appearance, 0.5f);

  object_selected_face_appearance = make_appearance();
  appearance_set_color (object_selected_face_appearance, 0.0f, 1.0f, 1.0f); /* 0.5f */
  appearance_set_alpha (object_selected_face_appearance, 0.5f);

  object_default_edge_appearance = make_appearance();
  appearance_set_color (object_default_edge_appearance, 0.0f, 0.0f, 0.0f); /* 1.0f */
  appearance_set_alpha (object_default_edge_appearance, 1.0f);
//  appearance_set_color (object_default_edge_appearance, 1.0f, 0.0f, 1.0f); /* 1.0f */
//  appearance_set_color (object_default_edge_appearance, 1.0f, 1.0f, 1.0f); /* 1.0f */
//  appearance_set_alpha (object_default_edge_appearance, 0.3f);

  object_debug_edge_appearance = make_appearance();
  appearance_set_color (object_debug_edge_appearance, 1.0f, 0.0f, 0.0f); /* 1.0f */
  appearance_set_alpha (object_debug_edge_appearance, 1.0f);

  object_selected_edge_appearance = make_appearance();
  appearance_set_color (object_selected_edge_appearance, 0.0f, 1.0f, 1.0f); /* 1.0f */
  appearance_set_alpha (object_selected_edge_appearance, 1.0f);

}

float colors[12][3] = {{1., 0., 0.},
                       {1., 1., 0.},
                       {0., 1., 0.},
                       {0., 1., 1.},
                       {0.5, 0., 0.},
                       {0.5, 0.5, 0.},
                       {0., 0.5, 0.},
                       {0., 0.5, 0.5},
                       {1., 0.5, 0.5},
                       {1., 1., 0.5},
                       {0.5, 1., 0.5},
                       {0.5, 1., 1.}};


#define CIRC_SEGS_D 64.0


struct draw_info {
  hidGC gc;
  object3d *object;
  bool selected;
  bool debug_face;
};


static void
draw_linearised (edge_ref e)
{
  edge_info *info = UNDIR_DATA(e);
  double lx, ly, lz;
  double x, y, z;
  int i;

  edge_ensure_linearised (e);

  glBegin (GL_LINES);

  for (i = 0; i < info->num_linearised_vertices; i++, lx = x, ly = y, lz = z)
    {
      x = info->linearised_vertices[i * 3 + 0];
      y = info->linearised_vertices[i * 3 + 1];
      z = info->linearised_vertices[i * 3 + 2];

      if (i > 0)
        {
          glVertex3f (MM_TO_COORD (lx), MM_TO_COORD (ly), MM_TO_COORD (lz));
          glVertex3f (MM_TO_COORD ( x), MM_TO_COORD ( y), MM_TO_COORD ( z));
        }
    }

  glEnd ();
}

static void
draw_quad_edge (edge_ref e, void *data)
{
  edge_info *info = UNDIR_DATA(e);
  struct draw_info *d_info = data;
#if 0
  int i;

  int id = ID(e) % 12;

  glColor3f (colors[id][0], colors[id][1], colors[id][2]);
#endif
#if 0
  if (d_info->selected)
    glColor4f (0.0, 1.0, 1., 1.0);
//    glColor4f (0.0, 1.0, 1., 0.5);
  else
    glColor4f (0., 0., 0., 1.0);
//    glColor4f (1., 1., 1., 0.3);
#endif

  if (info == NULL)
    return;

  if (!d_info->selected &&
      (info->is_placeholder ||
      d_info->debug_face))
    {
      glColor4f (1.0, 0.0, 0.0, 1.0);
      glDepthMask (TRUE);
      glDisable(GL_DEPTH_TEST);
    }

//  if (info->is_stitch)
//    return;

  draw_linearised (e);

    glEnable(GL_DEPTH_TEST);
//  glDepthMask (FALSE);
}

static void
draw_contour (contour3d *contour, void *data)
{
//  struct draw_info *info = data;
  edge_ref e;

  e = contour->first_edge;

  do
    {
      draw_quad_edge (e, data);

      /* LNEXT should take us counter-clockwise around the face */
    }
  while ((e = LNEXT(e)) != contour->first_edge);
}

static int face_no;

static void
draw_face_edges (face3d *face, void *data)
{
  struct draw_info *info = data;

#ifdef DEBUG_EDGE_HIGHLIGHT
  info->debug_face = (face_no == debug_integer);
#else
  info->debug_face = face->is_debug;
#endif

  if (face->is_debug)
    appearance_apply_gl (object_debug_edge_appearance);
  else if (info->selected)
    appearance_apply_gl (object_selected_edge_appearance);
//  else if (face->appearance)
//    appearance_apply_gl (face->apper);
//  else if (info->object)
//    appearance_apply_gl (info->object->apper);
  else
    appearance_apply_gl (object_default_edge_appearance);

  g_list_foreach (face->contours, (GFunc)draw_contour, info);

  face_no++;
}

static void
draw_face (face3d *face, void *data)
{
  struct draw_info *info = data;
  float white[] = {0.4f, 0.4f, 0.4f};
  float shininess[] = {10.0f};

//  face->is_debug = (face_no == debug_integer);

  if (face->is_debug)
    appearance_apply_gl (object_debug_face_appearance);
  else if (info->selected)
    appearance_apply_gl (object_selected_face_appearance);
  else if (face->appear)
    appearance_apply_gl (face->appear);
  else if (info->object->appear)
    appearance_apply_gl (info->object->appear);
  else
    appearance_apply_gl (object_default_face_appearance);

  /* Object inherited appearances? */

//  glDisable (GL_LIGHTING);

  glUseProgram (0);

  glMaterialfv (GL_FRONT_AND_BACK, GL_SPECULAR, white);
  glMaterialfv (GL_FRONT_AND_BACK, GL_SHININESS, shininess);

  face_no++;

//  if (!face->is_debug)
//    return;

  face3d_fill (info->gc, face, info->selected);
//  face3d_fill (info->gc, face, (face_no == debug_integer));

//  info->debug_face = (face_no == debug_integer);
//
//  return;
//
//  if (face->contours != NULL)
//      draw_contour (face->contours->data, info);
//  printf ("Drawing face\n");
//  g_list_foreach (face->contours, (GFunc)draw_contour, info);

}

/* This function is based on code from Game Programming Gems 1:
 * http://read.pudn.com/downloads32/sourcecode/game/105186/Game%20Programming%20Gems%201/Polygonal/01Lengyel/tweaking.cpp__.htm
 *
 * Portions Copyright (C) Eric Lengyel, 2000
 */

static void
push_and_tweak_projection (void)
{
  GLfloat matrix[16];
  GLfloat epsilon;

  glPushAttrib (GL_TRANSFORM_BIT);
  glMatrixMode(GL_PROJECTION);

  glPushMatrix ();

  // Retrieve the projection matrix
  glGetFloatv (GL_PROJECTION_MATRIX, matrix);

  // Calculate epsilon with equation (7)
//  epsilon = -2.0f * f * n * delta / ((f + n) * pz * (pz + delta));
//  epsilon = 4.8e-7;
  epsilon = 1e-5;

  // Modify entry (3,3) of the projection matrix
//  matrix[10] *= 1.0f + epsilon;
  matrix[10] += epsilon;

  // Send the projection matrix back to OpenGL
  glLoadMatrixf (matrix);
  glPopAttrib ();
}

static void
pop_projection (void)
{
  glPushAttrib (GL_TRANSFORM_BIT);
  glMatrixMode(GL_PROJECTION);
  glPopMatrix ();
  glPopAttrib ();
}

void
object3d_draw (hidGC gc, object3d *object, bool selected)
{
  struct draw_info info;

//  hidglGC hidgl_gc = (hidglGC)gc;
//  hidgl_instance *hidgl = hidgl_gc->hidgl;
//  hidgl_priv *priv = hidgl->priv;

  g_return_if_fail (object->edges != NULL);

  info.gc = gc;
  info.object = object;
  info.selected = selected;

//  quad_enum ((edge_ref)object->edges->data, draw_quad_edge, NULL);
//  printf ("BEGIN DRAW...\n");
//  g_list_foreach (object->edges, (GFunc)draw_quad_edge, NULL);

//  printf ("\nDrawing object\n");

  glDisable(GL_LIGHTING); /* XXX: HACK */

  face_no = 0;
  push_and_tweak_projection ();
  g_list_foreach (object->faces, (GFunc)draw_face_edges, &info);
  pop_projection ();

  glEnable(GL_LIGHTING); /* XXX: HACK */

  face_no = 0;
  g_list_foreach (object->faces, (GFunc)draw_face, &info);

//  printf ("....ENDED\n");
}

static void
object3d_draw_debug_single (object3d *object, void *user_data)
{
  hidGC gc = user_data;

  object3d_draw (gc, object, false);
}

void
object3d_draw_debug (hidGC gc)
{
  g_list_foreach (object3d_test_objects, (GFunc)object3d_draw_debug_single, gc);
}
