#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
//#include <stdarg.h> /* not used */
//#include <stdlib.h>
//#include <string.h>
//#include <assert.h>
//#include <time.h>

#include "global.h"
#include "data.h"
#include "misc.h"
//#include "pcb-printf.h"
//#include "create.h"

//#include "hid.h"
//#include "hid_draw.h"
//#include "../hidint.h"
//#include "hid/common/hidnogui.h"
//#include "hid/common/draw_helpers.h"
//#include "step.h"
//#include "hid/common/hidinit.h"
//#include "polygon.h"
//#include "misc.h"
//#include "rtree.h"
//#include "strflags.h"

#include "stackup.h"

#define NAUGHTY_CODE_DUPLICATION

typedef enum {
  PROCESS_DOWN_STACK = 0,
  PROCESS_UP_STACK,
} stack_dir_t;



#ifdef NAUGHTY_CODE_DUPLICATION
static bool
parse_position_attribute (AttributeListType *attrs, char *attr_name, double *res)
{
  const char *attr_value = AttributeGetFromList (attrs, attr_name);
  bool absolute;

  *res = 0.0;
  if (attr_value == NULL)
    return false;

  *res = COORD_TO_MM (GetValueEx (attr_value, NULL, &absolute, NULL, "cmil"));
  return true;
}
#endif


static bool
parse_integer_attribute (AttributeListType *attrs, char *attr_name, int *res)
{
  const char *attr_value = AttributeGetFromList (attrs, attr_name);

  *res = 0;
  if (attr_value == NULL)
    return false;

  *res = atoi (attr_value);
  return true;
}

static int
count_stack_items(void)
{
  int i;
  char *attr_name;

  for (i = 0; ; i++) {
    bool layer_found;

    attr_name = g_strdup_printf ("PCB::stackup::%i::thickness", i);
    layer_found = (AttributeGet (PCB, attr_name) != NULL);
    g_free (attr_name);

    if (!layer_found)
      break;
  }

  return i;
}


static double check_z_stacking_override(stackup_t *stackup,
                                        int stack_index,
                                        double current_z,
                                        stack_dir_t direction)
{
  char *attr_name[2];
  int above_index = 0;
  int below_index = 0;
  bool have_above_index;
  bool have_below_index;

  attr_name[0] = g_strdup_printf ("PCB::stackup::%i::above_stackup", stack_index);
  attr_name[1] = g_strdup_printf ("PCB::stackup::%i::above_stackup", stack_index);

  have_above_index = parse_integer_attribute (&PCB->Attributes, attr_name[0], &above_index);
  have_below_index = parse_integer_attribute (&PCB->Attributes, attr_name[1], &below_index);

  if (have_above_index && (
        above_index < 0 ||
        above_index >= stackup->item_count ||
        above_index == stack_index))
  {
    printf ("Bad above_index %i for stack intex %i (ignoring)\n", above_index, stack_index);
    goto out;
  }

  if (have_below_index && (
        below_index < 0 ||
        below_index >= stackup->item_count ||
        below_index == stack_index))
    {
      printf ("Bad below_index %i for stack intex %i (ignoring)\n", below_index, stack_index);
      goto out;
    }

  if (have_above_index && direction == PROCESS_DOWN_STACK && above_index > stack_index)
    {
      printf ("Processing stack in downward direction, found a above_stackup"
              "reference to an item with a greater numeric stack index (ignoring).\n");
      goto out;
    }

  if (have_below_index && direction == PROCESS_UP_STACK && below_index < stack_index)
    {
      printf ("Processing stack in upward direction, found a below_stackup"
              "reference to an item with a lower numeric stack index (ignoring).\n");
      goto out;
    }

  current_z = stackup->items[above_index].z_top;

out:
  g_free (attr_name[0]);
  g_free (attr_name[1]);

  return current_z;
}


stackup_t *
calculate_stackup (void)
{
  int i;
  int defined_z_stack_no;
  bool found_defined_z_stack = false;
  double current_z;
  stackup_t *stackup;

  stackup = g_new0 (stackup_t, 1);

  stackup->item_count = count_stack_items();
  printf ("Counted %i stack items\n", stackup->item_count);

  stackup->items = g_new0 (stack_item_t, stackup->item_count);

  // Forward pass, look for defiend z coord, process general attributes
  for (i = 0; i < stackup->item_count; i++)
    {
      char *attr_name;
      bool defined_z;

      attr_name = g_strdup_printf ("PCB::stackup::%i::thickness", i);
      parse_position_attribute (&PCB->Attributes, attr_name, &stackup->items[i].thickness);
      g_free (attr_name);

      attr_name = g_strdup_printf ("PCB::stackup::%i::name", i);
      stackup->items[i].name = g_strdup_printf ("%s", AttributeGet (PCB, attr_name));
      g_free (attr_name);

      attr_name = g_strdup_printf ("PCB::stackup::%i::color", i);
      stackup->items[i].color = g_strdup_printf ("%s", AttributeGet (PCB, attr_name));
      g_free (attr_name);


      attr_name = g_strdup_printf ("PCB::stackup::%i::z_centre", i);
      defined_z = parse_position_attribute (&PCB->Attributes, attr_name, &current_z);
      g_free (attr_name);

      if (defined_z) {
        if (found_defined_z_stack) {
          printf ("Oh dear - found additional stack item with defined z!\n");
        }
        stackup->items[i].z_top    = current_z - stackup->items[i].thickness / 2.0;
        stackup->items[i].z_bottom = current_z + stackup->items[i].thickness / 2.0;

        defined_z_stack_no = i;
        found_defined_z_stack = true;
      }

      attr_name = g_strdup_printf ("PCB::stackup::%i::layer_group", i);
      stackup->items[i].has_layer_group = parse_integer_attribute (&PCB->Attributes, attr_name, (int *)&stackup->items[i].layer_group);
      g_free (attr_name);

      attr_name = g_strdup_printf ("PCB::stackup::%i::outline_layer", i);
      stackup->items[i].has_outline_layer = parse_integer_attribute (&PCB->Attributes, attr_name, (int *)&stackup->items[i].outline_layer);
      g_free (attr_name);
    }

  if (!found_defined_z_stack) {
    printf ("Could not find stack item with defined z position\n");
    free_stackup(stackup);
    return NULL;
  }

  // Work back to top of stack

  current_z = stackup->items[defined_z_stack_no].z_top;

  for (i = defined_z_stack_no - 1; i >= 0; i--)
    {
      check_z_stacking_override(stackup, i, current_z, PROCESS_UP_STACK);

      stackup->items[i].z_top    = current_z - stackup->items[i].thickness;
      stackup->items[i].z_bottom = current_z;

      current_z = stackup->items[i].z_top;
    }

  // Work down to bottom of stack

  current_z = stackup->items[defined_z_stack_no].z_bottom;

  for (i = defined_z_stack_no + 1; i < stackup->item_count; i++)
    {
      check_z_stacking_override(stackup, i, current_z, PROCESS_DOWN_STACK);

      stackup->items[i].z_top    = current_z;
      stackup->items[i].z_bottom = current_z + stackup->items[i].thickness;

      current_z = stackup->items[i].z_bottom;
    }

  // Print stack list for debug
  for (i = 0; i < stackup->item_count; i++) {
    printf ("Stack item %i\n", i);
    printf ("  name = %s\n", stackup->items[i].name);
    printf ("  thickness = %f\n", stackup->items[i].thickness);
    printf ("  z_top    = %f\n", stackup->items[i].z_top);
    printf ("  z_bottom = %f\n", stackup->items[i].z_bottom);
    printf ("  color = %s\n", stackup->items[i].color);
    if (stackup->items[i].has_layer_group)
      printf ("  layer_group = %i\n", stackup->items[i].layer_group);
    if (stackup->items[i].has_outline_layer)
      printf ("  outline_layer = %i\n", stackup->items[i].outline_layer);
  }

  return stackup;
}


static void
free_stack_item (stack_item_t *item)
{
  g_free (item->name);
  g_free (item->color);
}


void
free_stackup (stackup_t *stackup)
{
  int i;

  if (stackup == NULL)
    return;

  for (i = 0; i < stackup->item_count; i++)
    free_stack_item (&stackup->items[i]);

  g_free (stackup->items);
  g_free (stackup);
}
