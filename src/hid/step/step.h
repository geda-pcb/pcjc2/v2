extern HID step_hid;
void step_init (HID *hid);
void step_graphics_init (HID_DRAW *graphics);
void step_load_models(Coord board_thickness);
