#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
//#include <stdarg.h> /* not used */
//#include <stdlib.h>
//#include <string.h>
#include <assert.h>
#include <time.h>

#include "global.h"
#include "data.h"
#include "misc.h"
//#include "error.h"
//#include "draw.h"
#include "pcb-printf.h"
#include "create.h"

#include "hid.h"
#include "hid_draw.h"
#include "../hidint.h"
#include "hid/common/hidnogui.h"
#include "hid/common/draw_helpers.h"
#include "step.h"
#include "hid/common/hidinit.h"
#include "polygon.h"
#include "rtree.h"
#include "strflags.h"

#include "hid/common/step_id.h"
#include "hid/common/quad.h"
#include "hid/common/vertex3d.h"
#include "hid/common/contour3d.h"
#include "hid/common/appearance.h"
#include "hid/common/face3d.h"
#include "hid/common/edge3d.h"
#include "hid/common/object3d.h"
#include "hid/common/stackup.h"
#include "object3d_step.h"

#ifdef HAVE_LIBDMALLOC
#include <dmalloc.h>
#endif

#include "model.h"
#include "assembly.h"

#define CRASH fprintf(stderr, "HID error: pcb called unimplemented STEP function %s.\n", __FUNCTION__); abort()

#define REVERSED_PCB_CONTOURS 1
//#undef REVERSED_PCB_CONTOURS

#ifdef REVERSED_PCB_CONTOURS
#define COORD_TO_STEP_X(pcb, x) (COORD_TO_MM(                   (x)))
#define COORD_TO_STEP_Y(pcb, y) (COORD_TO_MM((pcb)->MaxHeight - (y)))
#define COORD_TO_STEP_Z(pcb, z) (COORD_TO_MM(                   (z)))
#else
/* XXX: BROKEN UPSIDE DOWN OUTPUT */
#define COORD_TO_STEP_X(pcb, x) (COORD_TO_MM((x)))
#define COORD_TO_STEP_Y(pcb, y) (COORD_TO_MM((y)))
#define COORD_TO_STEP_Z(pcb, z) (COORD_TO_MM((z)))
#endif


static Coord board_thickness = 0;

HID step_hid;

HID_Attribute step_attribute_list[] = {
  /* other HIDs expect this to be first.  */

/* %start-doc options "91 STEP Export"
@ftable @code
@cindex stepfile
@item --stepfile <string>
Name of the STEP output file. Can contain a path.
@end ftable
%end-doc
*/
  {"stepfile", "STEP output file",
   HID_String, 0, 0, {0, 0, 0}, 0, 0},
#define HA_stepfile 0

  /* %start-doc options "91 STEP Export"
   @ftable @code
   @cindex copper
   @item --copper
   Export copper tracking
   @end ftable
   %end-doc
   */
    {"copper", N_("Export copper objects"),
         HID_Boolean, 0, 0, {0, 0, 0}, 0, 0},
#define HA_copper 1

  /* %start-doc options "91 STEP Export"
   @ftable @code
   @cindex solder-mask
   @item --solder-mask
   Export solder mask
   @end ftable
   %end-doc
   */
    {"soldermask", N_("Export soldermask"),
         HID_Boolean, 0, 0, {0, 0, 0}, 0, 0},
#define HA_soldermask 2

  /* %start-doc options "91 STEP Export"
   @ftable @code
   @cindex silk
   @item --silk
   Export silkscreen
   @end ftable
   %end-doc
   */
    {"silk", N_("Export silk"),
         HID_Boolean, 0, 0, {0, 0, 0}, 0, 0},
#define HA_silk 3

  /* %start-doc options "91 STEP Export"
   @ftable @code
   @cindex models
   @item --models
   Export component models
   @end ftable
   %end-doc
   */
    {"models", N_("Export component models"),
         HID_Boolean, 0, 0, {0, 0, 0}, 0, 0},
#define HA_models 4

  /* %start-doc options "91 STEP Export"
   @ftable @code
   @cindex thickness
   @item --thickness
   Board thickness
   @end ftable
   %end-doc
   */
    {"thickness", N_("Board thickness"),
         HID_Coord, 0, MM_TO_COORD(100), {0, 0, 0, MM_TO_COORD (1.6)}, 0, 0}, /* XXX: Arbitrary limit of 100mm thick PCB */
#define HA_thickness 5
};

#define NUM_OPTIONS (sizeof(step_attribute_list)/sizeof(step_attribute_list[0]))

REGISTER_ATTRIBUTES (step_attribute_list)

static HID_Attr_Val step_option_values[NUM_OPTIONS];

static HID_Attribute *
step_get_export_options (int *n)
{
  static char *last_made_filename = 0;
  if (PCB)
    derive_default_filename(PCB->Filename, &step_attribute_list[HA_stepfile], ".step", &last_made_filename);

  if (n)
    *n = NUM_OPTIONS;
  return step_attribute_list;
}

#if 0 // We haven't implemented using these yet!
/* NB: Result is in mm */
static void
parse_cartesian_point_3d_string (const char *str, double *x, double *y, double *z)
{
  *x = 0.0, *y = 0.0, *z = 0.0;
}

/* NB: Result is in mm */
static void
parse_direction_3d_string (const char *str, double *x, double *y, double *z)
{
  *x = 0.0, *y = 0.0, *z = 0.0;
}

/* NB: Result is in degrees */
static void
parse_rotation_string (const char *str, double *rotation)
{
  *rotation = 0.0;
}
#endif

static void
parse_position_attribute (ElementType *element, char *attr_name, double *res)
{
  const char *attr_value = AttributeGet (element, attr_name);
  bool absolute;

  *res = 0.0;
  if (attr_value == NULL)
    return;

  *res = COORD_TO_MM (GetValueEx (attr_value, NULL, &absolute, NULL, "cmil"));
}

static void
parse_numeric_attribute (ElementType *element, char *attr_name, double *res)
{
  const char *attr_value = AttributeGet (element, attr_name);
  bool absolute;

  *res = 0.0;
  if (attr_value == NULL)
    return;

  *res = COORD_TO_MM (GetValueEx (attr_value, NULL, &absolute, NULL, "mm")); /* KLUDGE */
}

static GList *loaded_models = NULL;

void
step_load_models(Coord board_thickness_)
{
  struct assembly_model *model;
  struct assembly_model_instance *instance;
  const char *attribute;

  board_thickness = board_thickness_;

  ELEMENT_LOOP (PCB->Data);
    {
      bool on_solder = TEST_FLAG (ONSOLDERFLAG, element);
      double on_solder_negate = on_solder ? -1.0 : 1.0;
      const char *model_filename;
      double ox, oy, oz;
      double ax, ay, az;
      double rx, ry, rz;
      double rotation;
      double cos_rot;
      double sin_rot;
      GList *model_iter;

      /* Skip if the component doesn't have a STEP-AP214 3d_model */
      attribute = AttributeGet (element, "PCB::3d_model::type");
      if (attribute == NULL || strcmp (attribute, "STEP-AP214") != 0)
        continue;

      attribute = AttributeGet (element, "PCB::3d_model::filename");
      if (attribute == NULL)
        continue;
      model_filename = attribute;

#if 0   /* Rather than write a parser for three floats in a string, separate X, Y, Z explicitly for quicker testing */

      attribute = AttributeGet (element, "PCB::3d_model::origin");
      if (attribute == NULL)
        continue;
      parse_cartesian_point_3d_string (attribute, &ox, &oy, &oz);

      attribute = AttributeGet (element, "PCB::3d_model::axis");
      if (attribute == NULL)
        continue;
      parse_direction_3d_string (attribute, &ax, &ay, &az);
      ax = 0.0, ay = 0.0, az = 1.0;

      attribute = AttributeGet (element, "PCB::3d_model::ref_dir");
      if (attribute == NULL)
        continue;
      parse_direction_3d_string (attribute, &rx, &ry, &rz);
      rx = 1.0, ry = 0.0, rz = 0.0;
#endif

      /* XXX: Should parse a unit suffix, e.g. "degrees" */
      attribute = AttributeGet (element, "PCB::rotation");
      if (attribute == NULL)
        continue;
#if 0   /* Rather than write a parser for angles, cheat below */
      parse_rotation_string (attribute, &rotation);
#endif

      /* XXX: QUICKER TO CODE INDIVIDULAL VALUES NOT SPACE SEPARATED */
      parse_position_attribute (element, "PCB::3d_model::origin::X", &ox);
      parse_position_attribute (element, "PCB::3d_model::origin::Y", &oy);
      parse_position_attribute (element, "PCB::3d_model::origin::Z", &oz);
      parse_numeric_attribute (element, "PCB::3d_model::axis::X", &ax);
      parse_numeric_attribute (element, "PCB::3d_model::axis::Y", &ay);
      parse_numeric_attribute (element, "PCB::3d_model::axis::Z", &az);
      parse_numeric_attribute (element, "PCB::3d_model::ref_dir::X", &rx);
      parse_numeric_attribute (element, "PCB::3d_model::ref_dir::Y", &ry);
      parse_numeric_attribute (element, "PCB::3d_model::ref_dir::Z", &rz);
      parse_numeric_attribute (element, "PCB::rotation", &rotation);

#if 1  /* Write the intended final syntax attributes */
      if (1)
        {
          GString *value = g_string_new (NULL);

          attribute = AttributeGet (element, "PCB::3d_model::origin::X");
          g_string_printf (value, "%s", attribute != NULL ? attribute : "0.0mm");
          attribute = AttributeGet (element, "PCB::3d_model::origin::Y");
          g_string_append_printf (value, " %s", attribute != NULL ? attribute : "0.0mm");
          attribute = AttributeGet (element, "PCB::3d_model::origin::Z");
          g_string_append_printf (value, " %s", attribute != NULL ? attribute : "0.0mm");
          AttributePutToList (&element->Attributes, "PCB::3d_model::origin", value->str, true);

          attribute = AttributeGet (element, "PCB::3d_model::axis::X");
          g_string_printf (value, "%s", attribute != NULL ? attribute : "0.0");
          attribute = AttributeGet (element, "PCB::3d_model::axis::Y");
          g_string_append_printf (value, " %s", attribute != NULL ? attribute : "0.0");
          attribute = AttributeGet (element, "PCB::3d_model::axis::Z");
          g_string_append_printf (value, " %s", attribute != NULL ? attribute : "0.0");
          AttributePutToList (&element->Attributes, "PCB::3d_model::axis", value->str, true);

          attribute = AttributeGet (element, "PCB::3d_model::ref_dir::X");
          g_string_printf (value, "%s", attribute != NULL ? attribute : "0.0");
          attribute = AttributeGet (element, "PCB::3d_model::ref_dir::Y");
          g_string_append_printf (value, " %s", attribute != NULL ? attribute : "0.0");
          attribute = AttributeGet (element, "PCB::3d_model::ref_dir::Z");
          g_string_append_printf (value, " %s", attribute != NULL ? attribute : "0.0");
          AttributePutToList (&element->Attributes, "PCB::3d_model::ref_dir", value->str, true);

          g_string_free (value, true);
        }
#endif

      instance = element->assembly_model_instance;

      if (instance != NULL)
        {
          if (strcmp (instance->model->filename, model_filename) == 0)
            {
              /* Same model */
            }
          else
            {
              /* Different model, delete the old instance */

              struct assembly_model *old_model;

              old_model = instance->model;
              old_model->instances = g_list_remove (old_model->instances, instance);
              g_free (instance);
              instance = NULL;

              /* Keep things clean, don't leave this pointer dangling - even temporarily */
              element->assembly_model_instance = NULL;

              if (old_model->instances == NULL)
                {
                  /* Assume unused, so unload */
                  if (old_model->step_model != NULL)
                    step_model_free (old_model->step_model);
                  g_free (old_model);
                  loaded_models = g_list_remove (loaded_models, old_model);
                }
            }
        }

      if (instance == NULL)
        {
          /* No model loaded yet */
          model = NULL;

          /* Look for prior usage of this model */
          for (model_iter = loaded_models;
               model_iter != NULL;
               model_iter = g_list_next (model_iter))
            {
              struct assembly_model *possible_model;
              possible_model = model_iter->data;
              if (strcmp (possible_model->filename, model_filename) == 0)
                {
                  model = possible_model;
                  break;
                }
            }

          /* If we didn't find this model used already, add it to the list */
          if (model == NULL)
            {
              model = g_new0 (struct assembly_model, 1);
              model->filename = model_filename;
              model->step_model = step_model_to_shape_master (model_filename);
              loaded_models = g_list_append (loaded_models, model);
            }
          instance = g_new0 (struct assembly_model_instance, 1);
          instance->model = model;
          model->instances = g_list_append (model->instances, instance);
          element->assembly_model_instance = instance;
        }

      cos_rot = cos (rotation * M_PI / 180.);
      sin_rot = sin (rotation * M_PI / 180.);

      // Rotation of part on board
      // (NB: Y flipped from normal right handed convention)
      //[cos -sin   0] [x] = [xcos - ysin]
      //[sin  cos   0] [y]   [xsin + ycos]
      //[  0    0   1] [z]   [z          ]

      // Flip of part to backside of board
      // [  1   0   0] [x] = [ x]
      // [  0  -1   0] [y] = [-y]
      // [  0   0  -1] [z] = [-z]


      instance->name = NAMEONPCB_NAME (element);
#ifdef REVERSED_PCB_CONTOURS
      instance->ox =                     ( ox * cos_rot + oy * sin_rot);
      instance->oy = -on_solder_negate * (-ox * sin_rot + oy * cos_rot);
      instance->oz = -on_solder_negate * oz; /* <--- ????: -ve on on_solder_negative seems inconsistent w.r.t. others! */
      instance->ax =                     ( ax * cos_rot + ay * sin_rot);
      instance->ay = -on_solder_negate * (-ax * sin_rot + ay * cos_rot);
      instance->az = -on_solder_negate * az;
      instance->rx =                     ( rx * cos_rot + ry * sin_rot);
      instance->ry = -on_solder_negate * (-rx * sin_rot + ry * cos_rot);
      instance->rz = -on_solder_negate * rz;

      // Saner maths verison (be explicit about the rotation in RHS coordinate system)
      // NB: Numerically, these should be identical to the above.
      instance->ox =                    (ox * cos_rot + -oy * -sin_rot); // <-- Flip oy -> -oy to correct PCB coord madness
      instance->oy = on_solder_negate * (ox * sin_rot + -oy *  cos_rot); // <-- Flip oy -> -oy to correct PCB coord madness
      instance->oz = on_solder_negate * -oz;                             // <-- Flip oz -> -oz to correct PCB coord madness
      instance->ax =                    (ax * cos_rot + -ay * -sin_rot);
      instance->ay = on_solder_negate * (ax * sin_rot + -ay *  cos_rot);
      instance->az = on_solder_negate * -az;
      instance->rx =                    (rx * cos_rot + -ry * -sin_rot);
      instance->ry = on_solder_negate * (rx * sin_rot + -ry *  cos_rot);
      instance->rz = on_solder_negate * -rz;
#else
      instance->ox =                     ( ox * cos_rot + oy * sin_rot);
      instance->oy =  on_solder_negate * (-ox * sin_rot + oy * cos_rot);
      instance->oz =  on_solder_negate * oz;
      instance->ax =                     ( ax * cos_rot + ay * sin_rot);
      instance->ay =  on_solder_negate * (-ax * sin_rot + ay * cos_rot);
      instance->az =  on_solder_negate * az;
      instance->rx =                     ( rx * cos_rot + ry * sin_rot);
      instance->ry =  on_solder_negate * (-rx * sin_rot + ry * cos_rot);
      instance->rz =  on_solder_negate * rz;

      // Saner maths verison (be explicit about the rotation in RHS coordinate system)
      // NB: Numerically, these should be identical to the above.
      instance->ox =                     (ox * cos_rot + -oy * -sin_rot);
      instance->oy = -on_solder_negate * (ox * sin_rot + -oy *  cos_rot);
      instance->oz = -on_solder_negate * -oz;
      instance->ax =                     (ax * cos_rot + -ay * -sin_rot);
      instance->ay = -on_solder_negate * (ax * sin_rot + -ay *  cos_rot);
      instance->az = -on_solder_negate * -az;
      instance->rx =                     (rx * cos_rot + -ry * -sin_rot);
      instance->ry = -on_solder_negate * (rx * sin_rot + -ry *  cos_rot);
      instance->rz = -on_solder_negate * -rz;
#endif
      instance->oy = -instance->oy; // XXX: WTF?

      instance->ox += COORD_TO_MM (element->MarkX); // Strange, kind of pseudo-pcb coordinate system, (origin + axis) - but in mm
      instance->oy += COORD_TO_MM (element->MarkY); // Strange, kind of pseudo-pcb coordinate system, (origin + axis) - but in mm
#ifdef REVERSED_PCB_CONTOURS
      instance->oz += COORD_TO_MM (on_solder ? -board_thickness : 0);
#else
      instance->oz += COORD_TO_MM (on_solder_negate * -board_thickness / 2);
#endif

    }
  END_LOOP;
}


static void
step_do_export (HID_Attr_Val * options)
{
  int i;
  const char *filename;
  const char *temp_pcb_filename = "_pcb.step";
  GList *board_outline_list;
  POLYAREA *board_outline;
  POLYAREA *piece;
  stackup_t *stackup;

  if (!options)
    {
      step_get_export_options (0);
      for (i = 0; i < NUM_OPTIONS; i++)
        step_option_values[i] = step_attribute_list[i].default_val;
      options = step_option_values;
    }

  board_thickness = options[HA_thickness].coord_value;

  stackup = calculate_stackup();

  /* If a stackup is defined, use that to set the board thickness */
  if (stackup != NULL && stackup->item_count > 0)
    {
      board_thickness = MM_TO_COORD (stackup->items[stackup->item_count - 1].z_bottom -
                                     stackup->items[0].z_top);
    }

  object3d_set_board_thickness (board_thickness);

  filename = options[HA_stepfile].str_value;
  if (filename == NULL)
    filename = "pcb-out.step";

  board_outline_list = object3d_from_board_outline ();

  board_outline = board_outline_poly (true);
  piece = board_outline;
  do {
    GList *silk_objects;
    GList *mask_objects;
    GList *copper_layer_objects;
    PLINE *curc;
    PLINE *next;
    PLINE **prev_next;

    // Remove any complete internal contours due to vias, so we may
    // more realistically show tented vias without loosing the ability
    // to split the soldermask body with a contour partly formed of vias.
    //
    // Should have the semantics that via holes in the mask are only due
    // to the clearance size, not the drill size - IFF they are on the
    // interior of a board body piece.
    //
    // If the via wall forms part of the board piece outside contour, the
    // soldermask will be the maximum of the drilling hole, or the clearance;
    // via drill-hole walls are not removed from the piece outside contour.

    prev_next = &piece->contours;
    for (curc = piece->contours; curc != NULL; curc = next)
      {
        next = curc->next;

        /* XXX: Insufficient test for via contour.. really need to KNOW this was a pin/via,
         *      as we may start using round tagged contours for circular cutouts etc...
         */
        if (!curc->is_round)
          {
            prev_next = &curc->next;
            continue;
          }

        /* Remove contour... */
        assert (*prev_next == curc);
        *prev_next = curc->next;
        curc->next = NULL;

        r_delete_entry (piece->contour_tree, (BoxType *) curc);
        poly_DelContour (&curc);
      }

#if 1
    if (options[HA_silk].int_value)
      {
        silk_objects = object3d_from_silk_within_area (stackup, piece, TOP_SIDE);
        board_outline_list = g_list_concat (board_outline_list, silk_objects);

        silk_objects = object3d_from_silk_within_area (stackup, piece, BOTTOM_SIDE);
        board_outline_list = g_list_concat (board_outline_list, silk_objects);
      }
#endif

#if 1
    if (options[HA_soldermask].int_value)
      {
        mask_objects = object3d_from_soldermask_within_area (stackup, piece, TOP_SIDE);
        board_outline_list = g_list_concat (board_outline_list, mask_objects);

        mask_objects = object3d_from_soldermask_within_area (stackup, piece, BOTTOM_SIDE);
        board_outline_list = g_list_concat (board_outline_list, mask_objects);
      }
#endif

#if 1
    if (options[HA_copper].int_value)
      {
        copper_layer_objects = object3d_from_copper_layers_within_area (stackup, piece);
        board_outline_list = g_list_concat (board_outline_list, copper_layer_objects);
      }
#endif

  } while ((piece = piece->f) != board_outline);
  poly_Free (&board_outline);

//  object3d_list_export_to_step_part (board_outline_list, temp_pcb_filename);
  object3d_list_export_to_step_assy (board_outline_list, temp_pcb_filename);
  g_list_free_full (board_outline_list, (GDestroyNotify)destroy_object3d);

  if (1) {
    GList *models = NULL;
    struct assembly_model *model;
    struct assembly_model_instance *instance;
    const char *attribute;

    model = g_new0 (struct assembly_model, 1);
    model->filename = temp_pcb_filename;
    models = g_list_append (models, model);

    instance = g_new0 (struct assembly_model_instance, 1);
    instance->model = model;
    instance->name = "PCB";
    instance->ox = 0.0,  instance->oy = 0.0,  instance->oz = 0.0;
    instance->ax = 0.0,  instance->ay = 0.0,  instance->az = 1.0;
    instance->rx = 1.0,  instance->ry = 0.0,  instance->rz = 0.0;
    model->instances = g_list_append (model->instances, instance);

    if (options[HA_models].int_value)
      {

        ELEMENT_LOOP (PCB->Data);
          {
            bool on_solder = TEST_FLAG (ONSOLDERFLAG, element);
            double on_solder_negate = on_solder ? -1.0 : 1.0;
            const char *model_filename;
            double ox, oy, oz;
            double ax, ay, az;
            double rx, ry, rz;
            double rotation;
            double cos_rot;
            double sin_rot;
            GList *model_iter;

            /* Skip if the component doesn't have a STEP-AP214 3d_model */
            attribute = AttributeGet (element, "PCB::3d_model::type");
            if (attribute == NULL || strcmp (attribute, "STEP-AP214") != 0)
              continue;

            attribute = AttributeGet (element, "PCB::3d_model::filename");
            if (attribute == NULL)
              continue;
            model_filename = attribute;

#if 0   /* Rather than write a parser for three floats in a string, separate X, Y, Z explicitly for quicker testing */

            attribute = AttributeGet (element, "PCB::3d_model::origin");
            if (attribute == NULL)
              continue;
            parse_cartesian_point_3d_string (attribute, &ox, &oy, &oz);

            attribute = AttributeGet (element, "PCB::3d_model::axis");
            if (attribute == NULL)
              continue;
            parse_direction_3d_string (attribute, &ax, &ay, &az);
            ax = 0.0, ay = 0.0, az = 1.0;

            attribute = AttributeGet (element, "PCB::3d_model::ref_dir");
            if (attribute == NULL)
              continue;
            parse_direction_3d_string (attribute, &rx, &ry, &rz);
            rx = 1.0, ry = 0.0, rz = 0.0;
#endif

            /* XXX: Should parse a unit suffix, e.g. "degrees" */
            attribute = AttributeGet (element, "PCB::rotation");
            if (attribute == NULL)
              continue;
#if 0   /* Rather than write a parser for angles, cheat below */
            parse_rotation_string (attribute, &rotation);
#endif

            /* XXX: QUICKER TO CODE INDIVIDULAL VALUES NOT SPACE SEPARATED */
            parse_position_attribute (element, "PCB::3d_model::origin::X", &ox);
            parse_position_attribute (element, "PCB::3d_model::origin::Y", &oy);
            parse_position_attribute (element, "PCB::3d_model::origin::Z", &oz);
            parse_numeric_attribute (element, "PCB::3d_model::axis::X", &ax);
            parse_numeric_attribute (element, "PCB::3d_model::axis::Y", &ay);
            parse_numeric_attribute (element, "PCB::3d_model::axis::Z", &az);
            parse_numeric_attribute (element, "PCB::3d_model::ref_dir::X", &rx);
            parse_numeric_attribute (element, "PCB::3d_model::ref_dir::Y", &ry);
            parse_numeric_attribute (element, "PCB::3d_model::ref_dir::Z", &rz);
            parse_numeric_attribute (element, "PCB::rotation", &rotation);

#if 1  /* Write the intended final syntax attributes */
            if (1)
              {
                GString *value = g_string_new (NULL);

                attribute = AttributeGet (element, "PCB::3d_model::origin::X");
                g_string_printf (value, "%s", attribute != NULL ? attribute : "0.0mm");
                attribute = AttributeGet (element, "PCB::3d_model::origin::Y");
                g_string_append_printf (value, " %s", attribute != NULL ? attribute : "0.0mm");
                attribute = AttributeGet (element, "PCB::3d_model::origin::Z");
                g_string_append_printf (value, " %s", attribute != NULL ? attribute : "0.0mm");
                AttributePutToList (&element->Attributes, "PCB::3d_model::origin", value->str, true);

                attribute = AttributeGet (element, "PCB::3d_model::axis::X");
                g_string_printf (value, "%s", attribute != NULL ? attribute : "0.0");
                attribute = AttributeGet (element, "PCB::3d_model::axis::Y");
                g_string_append_printf (value, " %s", attribute != NULL ? attribute : "0.0");
                attribute = AttributeGet (element, "PCB::3d_model::axis::Z");
                g_string_append_printf (value, " %s", attribute != NULL ? attribute : "0.0");
                AttributePutToList (&element->Attributes, "PCB::3d_model::axis", value->str, true);

                attribute = AttributeGet (element, "PCB::3d_model::ref_dir::X");
                g_string_printf (value, "%s", attribute != NULL ? attribute : "0.0");
                attribute = AttributeGet (element, "PCB::3d_model::ref_dir::Y");
                g_string_append_printf (value, " %s", attribute != NULL ? attribute : "0.0");
                attribute = AttributeGet (element, "PCB::3d_model::ref_dir::Z");
                g_string_append_printf (value, " %s", attribute != NULL ? attribute : "0.0");
                AttributePutToList (&element->Attributes, "PCB::3d_model::ref_dir", value->str, true);

                g_string_free (value, true);
              }
#endif

            printf ("Transform (%f, %f, %f), (%f, %f, %f), (%f, %f, %f). Rotation of part is %f\n", ox, oy, oz, ax, ay, az, rx, ry, rz, rotation);

            model = NULL;

            /* Look for prior usage of this model */
            for (model_iter = models;
                 model_iter != NULL;
                 model_iter = g_list_next (model_iter))
              {
                struct assembly_model *possible_model;
                possible_model = model_iter->data;
                if (strcmp (possible_model->filename, model_filename) == 0)
                  {
                    model = possible_model;
                    break;
                  }
              }

            /* If we didn't find this model used already, add it to the list */
            if (model == NULL)
              {
                model = g_new0 (struct assembly_model, 1);
                model->filename = model_filename;
                model->step_model = NULL; /* We don't use this for assembly export */
                models = g_list_append (models, model);
              }

            cos_rot = cos (rotation * M_PI / 180.);
            sin_rot = sin (rotation * M_PI / 180.);

            // Rotation of part on board
            // (NB: Y flipped from normal right handed convention)
            //[cos -sin   0] [x] = [xcos - ysin]
            //[sin  cos   0] [y]   [xsin + ycos]
            //[  0    0   1] [z]   [z          ]

            // Flip of part to backside of board
            // [  1   0   0] [x] = [ x]
            // [  0  -1   0] [y] = [-y]
            // [  0   0  -1] [z] = [-z]


            /* XXX: Note that here, instance coordinates are in the STEP model space.
             *      This is NOT the same as when we use this structure to describe
             *      instances of models on the PCB board, where we use PCB coordinate
             *      space (albiet in mm, not COOORD units).
             */

            instance = g_new0 (struct assembly_model_instance, 1);
            instance->model = model;
            instance->name = NAMEONPCB_NAME (element);
#ifdef REVERSED_PCB_CONTOURS
            instance->ox =                     ( ox * cos_rot + oy * sin_rot);
            instance->oy = -on_solder_negate * (-ox * sin_rot + oy * cos_rot);
            instance->oz = -on_solder_negate * oz; /* <--- ????: -ve on on_solder_negative seems inconsistent w.r.t. others! */
            instance->ax =                     ( ax * cos_rot + ay * sin_rot);
            instance->ay = -on_solder_negate * (-ax * sin_rot + ay * cos_rot);
            instance->az = -on_solder_negate * az;
            instance->rx =                     ( rx * cos_rot + ry * sin_rot);
            instance->ry = -on_solder_negate * (-rx * sin_rot + ry * cos_rot);
            instance->rz = -on_solder_negate * rz;
#else
            instance->ox =                     ( ox * cos_rot + oy * sin_rot);
            instance->oy =  on_solder_negate * (-ox * sin_rot + oy * cos_rot);
            instance->oz =  on_solder_negate * oz;
            instance->ax =                     ( ax * cos_rot + ay * sin_rot);
            instance->ay =  on_solder_negate * (-ax * sin_rot + ay * cos_rot);
            instance->az =  on_solder_negate * az;
            instance->rx =                     ( rx * cos_rot + ry * sin_rot);
            instance->ry =  on_solder_negate * (-rx * sin_rot + ry * cos_rot);
            instance->rz =  on_solder_negate * rz;
#endif

            instance->ox += COORD_TO_STEP_X (PCB, element->MarkX);
            instance->oy += COORD_TO_STEP_Y (PCB, element->MarkY);
#ifdef REVERSED_PCB_CONTOURS
            instance->oz += COORD_TO_STEP_Z (PCB, on_solder ? -board_thickness : 0);
#else
            instance->oz += COORD_TO_STEP_Z (PCB, on_solder_negate * -board_thickness / 2);
#endif

            model->instances = g_list_append (model->instances, instance);

          }
        END_LOOP;
      }

    export_step_assembly (filename, models);

    /* XXX: LEAK ALL THE MODEL DATA.. BEING LAZY RIGHT NOW */
  }

  free_stackup(stackup);

}

static void
normalise_vector (double x, double y, double z, double *xn, double *yn, double *zn)
{
  double recip_length = 1. / hypot (hypot (x, y), z);
  *xn = x * recip_length;
  *yn = y * recip_length;
  *zn = z * recip_length;
}

static void
project_vertex_to_uv (vertex3d *vertex,
                      double ux, double uy, double uz,
                      double vx, double vy, double vz,
                      double *u, double *v)
{
  *u = ux * vertex->x + uy * vertex->y + uz * vertex->z;
  *v = vx * vertex->x + vy * vertex->y + vz * vertex->z;
}

/*
 * NB: Angles in PCB units (degrees), underlying Angle type is double precision floating point
 */
static void
arc_parameters_from_edge (edge_ref edge,
                          double ux, double uy, double uz,
                          double vx, double vy, double vz,
                          double *cu, double *cv,
                          Angle *start_angle, Angle *delta_angle)
{
  edge_info *info = UNDIR_DATA(edge);
  vertex3d *v1 = ODATA (edge);
  vertex3d *v2 = DDATA (edge);
  vertex3d vc;
  double x1, y1, z1;
  double x2, y2, z2;
  double nx, ny, nz;
  double startx, starty, startz;
  double endx, endy, endz;
  double ortx, orty, ortz;
  double cosa;
  double sina;
  bool opposing_normal = false;

  vc.x = info->cx, vc.y = info->cy, vc.z = info->cz;
  project_vertex_to_uv (&vc, ux, uy, uz, vx, vy, vz, cu, cv);

  x1 = v1->x, y1 = v1->y, z1 = v1->z;
  x2 = v2->x, y2 = v2->y, z2 = v2->z;

  nx = info->nx;
  ny = info->ny;
  nz = info->nz;

  if (!info->same_sense)
    {
      nx = -nx;
      ny = -ny;
      nz = -nz;
    }

  opposing_normal = ((uy * vz - uz * vy) * nx +
                     (uz * vx - ux * vz) * ny +
                     (ux * vy - uy * vx) * nz) < 0.0;


  /* Normalised start vector */
  normalise_vector (x1 - info->cx,
                    y1 - info->cy,
                    z1 - info->cz, &startx, &starty, &startz);

  /* Normalised end vector */
  normalise_vector (x2 - info->cx,
                    y2 - info->cy,
                    z2 - info->cz, &endx, &endy, &endz);

  /* Calculate start angle w.r.t. u axis*/
  cosa = ux * startx + uy * starty + uz * startz; // cos (phi)
  sina = vx * startx + vy * starty + vz * startz; // sin (phi)

  *start_angle = 180.0 - 180.0 * atan2 (sina, cosa) / M_PI;
  if (*start_angle < 0.0)
    *start_angle += 360.0;

  /* start cross normal */
  /* ort will be orthogonal to normal and start vector */
  ortx = ny * startz - nz * starty;
  orty = nz * startx - nx * startz;
  ortz = nx * starty - ny * startx;

  /* Cosine is dot product of start (normalised) and end (normalised) */
  cosa = startx * endx + starty * endy + startz * endz; // cos (phi)
  /* Sine is dot product of ort (normalised) and end (normalised) */
  sina = ortx * endx + orty * endy + ortz * endz; // sin (phi) = cos (phi - 90)

  if (x1 == x2 &&
      y1 == y2 &&
      z1 == z2)
    {
      *delta_angle = 360.0;
    }
  else
    {
      /* Delta angled */
      *delta_angle = 180.0 * atan2 (sina, cosa) / M_PI;

      if (*delta_angle < 0.0)
        *delta_angle += 360.0;

      if (!opposing_normal)
        *delta_angle = -*delta_angle;
    }
}

static const char load_step_outline_to_buffer_syntax[] =
  "LoadStepOutlineToBuffer(filename[, [lines|polys]])";

static const char load_step_outline_to_buffer_help[] =
  "Loads a STEP model of a PCB, and attempts to extract the outline. \
   This outline is loaded to the buffer in PCB\'s best guess at the \
   design outline layer";

static gint
load_step_outline_to_buffer (int argc, char **argv, Coord x, Coord y)
{
  step_model *model;
  object3d *object;
  GList *face_iter;
  GList *contour_iter;
  double nx, ny, nz;
  double ux, uy, uz;
  double vx, vy, vz;
  bool have_start_point;
  bool have_last_point;
  double start_x = NAN; // Assign to NAN to stop compiler warning (and give a chance we notice if we did indeed use this uninitialised
  double start_y = NAN; // Assign to NAN to stop compiler warning (and give a chance we notice if we did indeed use this uninitialised
  double last_x = NAN;  // Assign to NAN to stop compiler warning (and give a chance we notice if we did indeed use this uninitialised
  double last_y = NAN;  // Assign to NAN to stop compiler warning (and give a chance we notice if we did indeed use this uninitialised
  int i;
  LayerType *output_layer = NULL;
  bool found_outline = false;
  bool polys_mode = false;
  bool circles_as_holes = false;

  if (argc < 1)
    return 1;

  /* XXX: Proper argument parser! */
  if ((argc > 1 && strcasecmp (argv[1], "polys") == 0) ||
      (argc > 2 && strcasecmp (argv[2], "polys") == 0))
    {
      polys_mode = true;
    }

  if ((argc > 1 && strcasecmp (argv[1], "holes") == 0) ||
      (argc > 2 && strcasecmp (argv[2], "holes") == 0))
    {
      circles_as_holes = true;
    }

  /* XXX: Hard-code default model import orientation for now */

  /* Normal of import plane */
#if 0
  nx = 0.0, ny = 1.0, nz = 0.0; /* Typical of my Solidworks PCB models, drawin on the "Top" plane (X-Z) */
#else
  nx = 0.0, ny = 0.0, nz = -1.0; /* As exported by PCB's own STEP outline export plane (X-Y) */
#endif

  /* Reference axis direction in import plane (must be orthogonal to (nx, ny, nz) */
  ux = 1.0, uy = 0.0, uz = 0.0;

  /* ux cross normal */
  /* vx will be orthogonal to normal and ux vector */
  vx = ny * uz - nz * uy;
  vy = nz * ux - nx * uz;
  vz = nx * uy - ny * ux;

  output_layer = &PASTEBUFFER->Data->Layer[INDEXOFCURRENT];

  if (!polys_mode)
    {
      /* Find the outline layer (XXX - should we always just use the current selected layer? */

      /* XXX: CODE FUNCTIONALITY DUPLICATION WITH polygon.c OUTLINE GENERATION */
      for (i = 0; i < max_copper_layer; i++)
        {
          LayerType *Layer = PCB->Data->Layer + i;

          if (strcmp (Layer->Name, "outline") == 0 ||
              strcmp (Layer->Name, "route") == 0)
            {
              /* Use the corresponding layer in the buffer */
              found_outline = true;
              output_layer = PASTEBUFFER->Data->Layer + i;
              break;
            }
        }

      if (!found_outline)
        {
          printf ("Didn't find outline, defaulting to current layer\n");
        }
    }

  model = step_model_to_shape_master (argv[0]);

  object = model->object;
  /* XXX: What if the model coordinates are set oddly? */

  if (abs (model->ox - 0.0) > 0.000001 ||
      abs (model->oy - 0.0) > 0.000001 ||
      abs (model->oz - 0.0) > 0.000001 ||
      abs (model->ax - 0.0) > 0.000001 ||
      abs (model->ay - 0.0) > 0.000001 ||
      abs (model->az - 1.0) > 0.000001 ||
      abs (model->rx - 1.0) > 0.000001 ||
      abs (model->ry - 0.0) > 0.000001 ||
      abs (model->rz - 0.0) > 0.000001)
    {
      printf ("XXX: Will model load in wrong place?\n");
      printf ("ox=%f, oy=%f, oz=%f\n", model->ox, model->oy, model->oz);
      printf ("ax=%f, ay=%f, az=%f\n", model->ax, model->ay, model->az);
      printf ("rx=%f, ry=%f, rz=%f\n", model->rx, model->ry, model->rz);
    }

  for (face_iter = object->faces;
       face_iter != NULL;
       face_iter = g_list_next (face_iter))
    {
      face3d *face = face_iter->data;
      POLYAREA *np = NULL;

      /* Need to work out whether we like this face for the top or not? */

      if (!face->is_planar ||
          face->is_cylindrical ||
          face->is_conical ||
          face->is_spherical)
        {
          /* This face is not our nice planar surface */
          //printf ("Skipping non-planar face\n");
          continue;
        }

      /* This face "might" be the one we want. */

      /* For sake of prototyping speed, lets just test against a
       * known axis direction! (Must match our models)
       */

      if (abs (face->nx - nx) > 0.000001 ||
          abs (face->ny - ny) > 0.000001 ||
          abs (face->nz - nz) > 0.000001)
        {
          //printf ("Skipping face pointing in the wrong normal direction\n");
          continue;
        }

      /* Now we could traverse the connected edges to this face and test
       * to ensure orthogonality if we wished.
       *
       * We could also (if we wished), check that for each adjacent face,
       * it has four edges, and the face adjacent to the second edge in its
       * loop (IE - the bottom of the board) matches our plane, and has normal
       * pointing in the opposite direction.
       *
       * If we wanted (and PCB supported saving this information), we could
       * extract the PCB thickness by comparing the distance between these
       * two planes.
       */

      //printf ("FACE\n");

      if (polys_mode)
        {
          if ((np = poly_Create ()) == NULL)
            return 1;
        }

      /* Loop over contours of this face (might be holes etc.. */
      for (contour_iter = face->contours;
           contour_iter != NULL;
           contour_iter = g_list_next (contour_iter))
        {
          contour3d *contour = contour_iter->data;
          edge_ref edge = contour->first_edge;
          PLINE *ncontour = NULL;

          /* Loop over all edges of this face */

          have_start_point = false;
          have_last_point = false;

          //printf ("CONTOUR\n");

          do
            {
              edge_info *info = UNDIR_DATA (edge);
              vertex3d *v1 = ODATA (edge);
              vertex3d *v2 = DDATA (edge);
              bool backwards_edge;
              bool complete_circle;

              //printf ("EDGE\n");

              /* XXX: Do this without breaking abstraction? */
              /* Detect SYM edges, reverse the circle normal */
              backwards_edge = ((edge & 2) == 2);

              complete_circle = info->is_round &&
                                v1->x == v2->x &&
                                v1->y == v2->y &&
                                v1->z == v2->z; // NB: Assumes for circular contours, the double quantities will be _exatly_ equal

              if (info->is_round && (!polys_mode || (circles_as_holes && complete_circle)))
                {
                  double u, v;
                  double cx, cy;
                  Angle start_angle;
                  Angle delta_angle;

                  if (backwards_edge)
                    printf ("**** backwards_edge arc\n");

                  arc_parameters_from_edge (edge, ux, uy, uz, vx, vy, vz,
                                            &cx, &cy, &start_angle, &delta_angle);

                  if (circles_as_holes && complete_circle) /* (Also applies in poly mode) */
                    {
                      /* Add this hole to the board */
                      CreateNewVia (PASTEBUFFER->Data,
                                    MM_TO_COORD (cx), MM_TO_COORD (cy),
                                    MM_TO_COORD (2.0 * info->radius) /* via thickness */,
                                    MIL_TO_COORD (10) /* clearance */,
                                    MM_TO_COORD (2.0 * info->radius), /* mask */
                                    MM_TO_COORD (2.0 * info->radius) /* via drill size */,
                                    NULL, /* pin number */
                                    MakeFlags(HOLEFLAG));
                      /* XXX: UNDO SYSTEM? */
                    }
                  else /* NB: From enclosing test and main condition on if statement, we are not in poly mode */
                    {

                      project_vertex_to_uv (v1, ux, uy, uz, vx, vy, vz, &u, &v);

                      if (!have_start_point)
                        {
                          start_x = u, start_y = v;
                          have_start_point = true;
                        }

                      /* Add any remaining last segment of the previous PWL approximated edge */
                      if (have_last_point)
                        {
                          /* Add this segment of line to the outline layer */
                          CreateNewLineOnLayer (output_layer,
                                                MM_TO_COORD (last_x), MM_TO_COORD (last_y),
                                                MM_TO_COORD (u),      MM_TO_COORD (v),
                                                MIL_TO_COORD (10) /* thickness */, MIL_TO_COORD (10) /* clearance */,
                                                NoFlags());
                          /* XXX: UNDO SYSTEM? */
                        }

                      /* Add this segment of arc to the outline layer */
                      CreateNewArcOnLayer (output_layer,
                                           MM_TO_COORD (cx), MM_TO_COORD (cy),
                                           MM_TO_COORD (info->radius), MM_TO_COORD (info->radius),
                                           start_angle, delta_angle,
                                           MIL_TO_COORD (10) /* thickness */, MIL_TO_COORD (10) /* clearance */,
                                           NoFlags());
                      /* XXX: UNDO SYSTEM? */

                      have_last_point = false; /* false, since we already dealt with the end-point of this edge */
                    }

                }
              else
                {
                  double u, v;

                  edge_ensure_linearised (edge);

                  for (i = 0; i < info->num_linearised_vertices - 1; i++)
                    {
                      int vertex_idx = i;
                      vertex3d vertex;

                      if (backwards_edge)
                        vertex_idx = info->num_linearised_vertices - 1 - i;

                      vertex.x = info->linearised_vertices[vertex_idx * 3 + 0];
                      vertex.y = info->linearised_vertices[vertex_idx * 3 + 1];
                      vertex.z = info->linearised_vertices[vertex_idx * 3 + 2];

                      project_vertex_to_uv (&vertex, ux, uy, uz, vx, vy, vz, &u, &v);

                      if (!have_start_point)
                        {
                          start_x = u, start_y = v;
                          have_start_point = true;
                        }

                      if (polys_mode)
                        {
                          Vector nv;
                          VNODE *node;

                          nv[0] = MM_TO_COORD (u);
                          nv[1] = MM_TO_COORD (v);
                          node = poly_CreateNode (nv);

                          if (ncontour == NULL)
                            ncontour = poly_NewContour (node);
                          else
                            poly_InclVertex (ncontour->head.prev, node);
                        }
                      else
                        {
                          if (have_last_point)
                            {
                              /* Add this segment of line to the outline layer */
                              CreateNewLineOnLayer (output_layer,
                                                    MM_TO_COORD (last_x), MM_TO_COORD (last_y),
                                                    MM_TO_COORD (u),      MM_TO_COORD (v),
                                                    MIL_TO_COORD (10) /* thickness */, MIL_TO_COORD (10) /* clearance */,
                                                    NoFlags());
                              /* XXX: UNDO SYSTEM? */
                            }
                        }

                      last_x = u, last_y = v;
                      have_last_point = true;

                    }
                }
            }
          while ((edge = LNEXT(edge)) != contour->first_edge);

          if (polys_mode)
            {
              /* NB: We assume the "outer" outline comes first, holes second.
               *     We also assume that the order of point definition will be
               *     correct for both as a result of the source of the shape data.
               */
              if (ncontour != NULL)
              {
                //printf ("Pre-processing contour %p\n", ncontour);
                poly_PreContour (ncontour, TRUE);
//                poly_InvContour (ncontour);
                //printf ("Including contour in polygon\n");
                poly_InclContour (np, ncontour);
                //printf ("np->contours == %p\n", np->contours);
              }
            }
          else
            {
              if (have_last_point)
                {
                  /* Add this segment of line to the outline layer */
                  CreateNewLineOnLayer (output_layer,
                                        MM_TO_COORD (last_x),  MM_TO_COORD (last_y),
                                        MM_TO_COORD (start_x), MM_TO_COORD (start_y),
                                        MIL_TO_COORD (10) /* thickness */, MIL_TO_COORD (10) /* clearance */,
                                        NoFlags());
                  /* XXX: UNDO SYSTEM? */
                }
            }

        }

      if (polys_mode)
        {
          if (np == NULL)
            printf ("np == NULL\n");
          if (np->contours == NULL)
            printf ("np->contours == NULL\n");

          if (np != NULL && np->contours != NULL)
            PolyToPolygonsOnLayer (PASTEBUFFER->Data, output_layer, np, string_to_pcbflags ("clearpoly", NULL));
        }

      /* We found the face we wanted to play with, played with it.
       * No need to continue */
//      break;

    }

  step_model_free (model);

  return 0;
}

HID_Action step_action_list[] = {
  {"LoadSTEPOutlineToBuffer", 0, load_step_outline_to_buffer,
   load_step_outline_to_buffer_help, load_step_outline_to_buffer_syntax}
  ,
};

REGISTER_ACTIONS (step_action_list)

static void
step_parse_arguments (int *argc, char ***argv)
{
  hid_register_attributes (step_attribute_list, NUM_OPTIONS);
  hid_parse_command_line (argc, argv);
}

#include "dolists.h"

void step_step_init (HID *hid)
{
  hid->get_export_options = step_get_export_options;
  hid->do_export          = step_do_export;
  hid->parse_arguments    = step_parse_arguments;
}

void
hid_step_init ()
{
  memset (&step_hid, 0, sizeof (HID));

  common_nogui_init (&step_hid);
  step_step_init (&step_hid);

  step_hid.struct_size        = sizeof (HID);
  step_hid.name               = "step";
  step_hid.description        = "STEP AP214 export";
  step_hid.exporter           = 1;

  hid_register_hid (&step_hid);

#include "step_lists.h"
}
