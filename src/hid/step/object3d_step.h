void object3d_list_export_to_step_part (GList *objects, const char *filename);
void object3d_list_export_to_step_assy (GList *objects, const char *filename);
void object3d_export_to_step (object3d *object, const char *filename);
